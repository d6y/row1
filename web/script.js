"use strict";

// Terminology in the code:
// 
// We follow the naming using in spreadsheets, generally, in the code.
// What we show to users may be different.
//
// row            - one page of information
// column         - one field on a page
// column heading (or header) - the label for a field
// column name    - the identifier for a column such as "col-2"
// row name       - the identifier for a row, such as "row-1"
// row id         - the row number such as 1
//

// Representation of the current row and effects to manipulate the row
class State {
    constructor(row_id) {
        this.current_id = row_id;
    }

    show(row_id) {
        let row_to_show = document.getElementById("row-"+row_id);
        if (row_to_show) {
            document.getElementById("row-"+this.current_id).style.display = "none";
            row_to_show.style.display = "block";
            return new State(row_id);
        } else {
            return this;
        }
    }

    show_next_row() {
        return this.show(this.current_id + 1);
    }

    show_previous_row() {
        return (this.current_id > 1) ? this.show(this.current_id-1) : this;
    }
}

function bind_click(elem, fn) {
    if (elem) { elem.addEventListener("click", fn); }
}

function init() {

    //
    // Row navigation
    //
    
    let state = new State(1),
        next_row = () => state = state.show_next_row(),
        previous_row = () => state = state.show_previous_row();

    bind_click(document.getElementById("previous_row"), previous_row);
    bind_click(document.getElementById("next_row"), next_row);

    document.addEventListener('keypress', (event) => {
        switch (event.key) {
            case "ArrowRight": 
                next_row();
                break;
            case "ArrowLeft":
                previous_row();
                break;
        }
    });

    //
    // Properties inspector
    //

    let replace_checkbox = (checkbox_id, field_selector) => {
        console.log(checkbox_id, field_selector);
        let checkbox = document.getElementById(checkbox_id);
        let divs = document.getElementsByClassName(field_selector);

        let show_field_checked = divs[0].style.display == "none" ? false : true;
        checkbox.checked = show_field_checked;

        checkbox.onchange = (event) => {
            let display_style = event.target.checked ? "block" : "none";
            Array.prototype.forEach.call(divs, div => div.style.display = display_style);
        } 
    };

    let show_properties = (column_div) => {

        // Inspector title is the column heading:
        let label = column_div.children[0].textContent;
        document.getElementById("prop-heading").textContent = label;

        let col_name = column_div.dataset.columnName; // e.g., "col-1"

        // Bind behaviour and set checked/unchecked for checkboxes:
        replace_checkbox("prop-show-column-checkbox", "column "+col_name);
        replace_checkbox("prop-show-heading-checkbox", "column-header "+col_name);


        // Show the properties inspector:
        let prop_inspector = document.getElementById("properties");
        UIkit.offcanvas(prop_inspector).show();
    };

    let column_click_handler = (event) => {
        // If we've clicked near a column div, show the properties inspector for the column.
        let div = event.target.closest('div');
        if (div && div.classList && div.classList.contains("column")) {
            show_properties(div);
        }
    };

    document.body.addEventListener('click', column_click_handler);
}

window.onload = init;

