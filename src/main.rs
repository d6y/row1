#[macro_use]
extern crate horrorshow;
use horrorshow::helper::doctype;
use horrorshow::prelude::*;

extern crate csv;

use std::env;
use std::error::Error;
use std::ffi::OsString;
use std::fs::File;
use std::io::Write;

type Record = Vec<String>;
type Headers = csv::StringRecord;
type ColumnIDs = Vec<String>;

fn run(file_path: OsString) -> Result<String, Box<Error>> {
    let file = File::open(file_path)?;
    let mut rdr = csv::Reader::from_reader(file);

    let headers = rdr.headers()?.clone();
    let num_cols = headers.len();
    let column_ids: Vec<String> = (1..num_cols + 1).map(|i| format!("col-{}", i)).collect();

    let mut articles: Vec<String> = Vec::new();
    for (row_num, result) in rdr.deserialize().enumerate() {
        let record: Record = result?;
        let row_label = format!("row-{}", row_num + 1);
        let frag = to_article(&row_label, &column_ids, &headers, &record);
        articles.push(frag);
    }

    Ok(format!(
        "{}",
        html! {
            : doctype::HTML;
            html(lang="en") {
                head {
                    title : "Title here";
                    meta(charset="utf-8");
                    link(rel="stylesheet", href="uikit-3.0.0-rc.9/css/uikit.min.css");
                    link(rel="stylesheet", href="style.css");
                }
                body(class="uk-container") {
                    input(type="button", value="Previous", id="previous_row");
                    input(type="button", value="Next", id="next_row");
                    div(id="properties", uk-offcanvas="flip: true; bg-close: false") {
                        div(class="uk-offcanvas-bar") {
                            button(class="uk-offcanvas-close", type="button", uk-close) {}
                            h3(id="prop-heading") : "Title" ;
                            form {
                                fieldset(class="uk-fieldset", id="prop-visibility-set") {
                                        label(class="uk-form-label") {
                                            input(id="prop-show-column-checkbox", class="uk-checkbox", type="checkbox", name="show_column", checked="true") ;
                                            : "Show this field" ;
                                        }
                                        br ;

                                        label(class="uk-form-label") {
                                            input(id="prop-show-heading-checkbox", class="uk-checkbox", type="checkbox", name="show_column_header", checked="true") ;
                                            : "Show field heading" ;
                                        }
                                }
                            }
                        }
                    }
                    @ for article in articles {
                        : Raw(article) ;
                    }
                    script(src="uikit-3.0.0-rc.9/js/uikit.min.js") { : ""; }
                    script(src="uikit-3.0.0-rc.9/js/uikit-icons.min.js") { : ""; }
                    script(src="script.js") { : ""; }
                }
            }
        }.into_string()
            .unwrap()
    ))
}

fn to_article(
    row_label: &str,
    column_ids: &ColumnIDs,
    headers: &Headers,
    record: &Record,
) -> String {
    let iter = headers.iter().zip(record).zip(column_ids);

    format!(
        "{}",
        html! {
            article(id=row_label) {
                h1 : row_label ;
                    dl {
                        @ for ((k, v), i) in iter {
                            div(class=format!("column {}", i), data-column-name=i) {
                                dt(class=format!("column-header {}", i)) { : k.to_string(); }
                                dd(class=format!("column-value {}", i)) { : v.to_string(); }
                        }
                    }
                }
            }
        }.into_string()
            .unwrap()
    )
}

fn write_html(filename: &str, page: String) -> std::io::Result<()> {
    let mut file = File::create(filename)?;
    write!(file, "{}", page)?;
    Ok(())
}

fn get_first_arg() -> Result<OsString, Box<Error>> {
    match env::args_os().nth(1) {
        None => Err(From::from("expected CSV file name as an agument")),
        Some(file_path) => Ok(file_path),
    }
}

fn main() {
    let result = get_first_arg().and_then(run);
    let out_filename = String::from("web/output.html");

    match result {
        Err(err) => eprintln!("{}", err),
        Ok(html) => println!("{:?}", write_html(&out_filename, html)),
    }
}
